#include <iostream>
#include <fstream>
#include <string>
#include <iomanip>
#include <string>

using namespace std;

struct Adresse
{
	char name[30];
	char vorname[30];
	char strasse[30];
	char plz[10];
	char ort[30];
	int id;
	Adresse* vor;
	Adresse* nach;
};

void printMenu()
{
	cout << "Willkommen zur Adressverwaltung!" << endl;
	cout << "--------------------------------" << endl;
	cout << "\nWas moechten Sie tun?" << endl;
	cout << "1. Neue Adresse hinzufuegen" << endl;
	cout << "2. Eine Adresse aendern" << endl;
	cout << "3. Eine Adresse loeschen" << endl;
	cout << "4. Alle Adressen ausgeben" << endl;
	cout << "0. Programm beenden" << endl;
}

void readFile(Adresse* Kopf)
{
	Adresse* Laeufer, * Dummy;
	Laeufer = Kopf;
	Dummy = Laeufer;
	Laeufer = Laeufer->nach;

	ifstream lesen("Adressliste.txt");
	char kette[256];
	int i = 0;

	while (!lesen.eof())
	{
		Laeufer = new Adresse;
		Dummy->nach = Laeufer;
		Laeufer->vor = Dummy;
		Laeufer->nach = nullptr;

		
		lesen.getline(kette, 256, ';');
		if (sizeof(kette) > 0)
		{
			strcpy_s(Laeufer->name, kette);
			lesen.getline(kette, 256, ';');
			strcpy_s(Laeufer->vorname, kette);
			lesen.getline(kette, 256, ';');
			strcpy_s(Laeufer->strasse, kette);
			lesen.getline(kette, 256, ';');
			strcpy_s(Laeufer->plz, kette);
			lesen.getline(kette, 256, ';');
			strcpy_s(Laeufer->ort, kette);
			lesen.getline(kette, 256, ';');
			Laeufer->id = *kette;

		}

		Dummy = Laeufer;
		Laeufer = Laeufer->nach;

		i++;
	}
}

void printAllAdress(Adresse* Kopf)
{
	cout << "### Alle Adressen:" << endl;
	cout << "##################" << endl << endl;

	Adresse* Laeufer;
	Laeufer = Kopf;

	// Tabellenkopf
	/*
	cout << setw(5) << setfill(' ') << left << "Name"
		<< setw(25) << setfill(' ') << left << "Vorname"
		<< setw(12) << setfill(' ') << left << "Adresse"
		<< setw(7) << setfill(' ') << left << "PLZ"
		<< setw(20) << setfill(' ') << left << "Ort" << endl;
	cout << "--------------------------------------------------------------" << endl;
	*/
	// Zeilen
	while (Laeufer->nach) {
		Laeufer = Laeufer->nach;
		/*
		cout << setw(5) << setfill(' ') << left << Laeufer->name
			<< setw(25) << setfill(' ') << left << Laeufer->vorname
			<< setw(12) << setfill(' ') << left << Laeufer->strasse
			<< setw(7) << setfill(' ') << left << Laeufer->plz
			<< setw(20) << setfill(' ') << left << Laeufer->ort << endl;
			*/

		cout << Laeufer->name << endl;
		cout << Laeufer->vorname << endl;
		cout << Laeufer->strasse << endl;
		cout << Laeufer->plz << endl;
		cout << Laeufer->ort << endl;
		cout << Laeufer->id << endl << endl;
	}
}

void saveIntoFile(Adresse* Kopf)
{
	Adresse* Laeufer, * Dummy;
	Laeufer = Kopf;

	ofstream fileWriter;
	fileWriter.open("temp.txt");

	while (Laeufer->nach)
	{
		Laeufer = Laeufer->nach;
		fileWriter << Laeufer->name << ";" << Laeufer->vorname << ";" << Laeufer->strasse << ";" << Laeufer->plz << ";" << Laeufer->ort << ";" << Laeufer->id << ";";
	}

	fileWriter.close();
	remove("Adressliste.txt");
	rename("temp.txt", "Adressliste.txt");
}


void addAdress(Adresse* Kopf)
{
	cin.clear();
	cin.ignore(numeric_limits<streamsize>::max(), '\n');

	Adresse* Laeufer, * Dummy;
	Laeufer = Kopf;

	do {
		Dummy = Laeufer;
		Laeufer = Laeufer->nach;
	} while (Laeufer);

	Laeufer = new Adresse;
	Dummy->nach = Laeufer;
	Laeufer->vor = Dummy;
	Laeufer->nach = nullptr;

	Laeufer->id = rand() % 1000;

	//std::cin.clear();
	cout << "Bitte Namen eingeben: ";
	cin.getline(Laeufer->name, 256);
	cout << "Bitte Vornamen eingeben: ";
	cin.getline(Laeufer->vorname, 256);
	cout << "Bitte Strasse und Hausnummer eingeben: ";
	cin.getline(Laeufer->strasse, 256);
	cout << "Bitte Postleitzahl eingeben: ";
	cin.getline(Laeufer->plz, 256);
	cout << "Bitte Stadt eingeben: ";
	cin.getline(Laeufer->ort, 256);


	cout << endl << "### Neue Adresse wurde erfolgreich hinzugefuegt!" << endl << endl;

}

void changeAdress(Adresse* Kopf)
{
	Adresse* Laeufer, * Dummy;
	Laeufer = Kopf;
	int changeDecision = 0;

	int toChange;
	ofstream tempFile;
	tempFile.open("temp.txt", std::ofstream::out);

	printAllAdress(Kopf);

	cout << "\nGeben Sie die ID ein, bei dem Sie den Eintrag aendern wollen. " << endl;
	cin >> toChange;


	while (Laeufer->nach)
	{
		Laeufer = Laeufer->nach;
		if (Laeufer->id == toChange)
		{
			break;

		}
	}


	while (true)
	{

		cout << "Was wollen Sie aendern? " << endl;
		cout << "1. Den Namen" << endl;
		cout << "2. Den Vornamen" << endl;
		cout << "3. Die Adresse" << endl;
		cout << "4. Die Postleitzahl" << endl;
		cout << "5. Den Ort" << endl;
		cout << "0. Nichts weiteres." << endl;

		cin >> changeDecision;

		switch (changeDecision)
		{
		case 1:
			cout << "Geben Sie den neuen Namen ein." << endl;
			cin >> Laeufer->name;
			break;

		case 2:
			cout << "Geben Sie den neuen Vornamen ein." << endl;
			cin >> Laeufer->vorname;
			break;

		case 3:
			cout << "Geben Sie die neue Adresse ein." << endl;
			cin >> Laeufer->strasse;
			break;

		case 4:
			cout << "Geben Sie die neue Postleitzahl ein" << endl;
			cin >> Laeufer->plz;
			break;

		case 5:
			cout << "Geben Sie den neuen Ort ein." << endl;
			cin >> Laeufer->ort;
			break;

		default:
			break;
		}
		if (changeDecision == 0)
		{
			break;
		}
	}
}

void deleteAdress(Adresse* Kopf)
{
	bool adressFound = false;
	string toDelete;
	string temp;

	ofstream tempFile;
	tempFile.open("temp.txt");

	cout << "Bitte geben Sie den Namen ein um den gewuenschten Eintrag zu loeschen." << endl;
	cin >> toDelete;

	fstream deleteEntry("Adressliste.txt");

	while (getline(deleteEntry, temp))
	{
		if (temp.substr(0, toDelete.size()) == toDelete)
		{
			adressFound = true;
		}
		if (temp.substr(0, toDelete.size()) != toDelete)
		{
			tempFile << temp << endl;
		}
	}

	deleteEntry.close();
	tempFile.close();
	remove("Adressliste.txt");
	rename("temp.txt", "Adressliste.txt");

	if (adressFound == true)
	{
		cout << "\nDie Adresse von " << toDelete << " wurde erfolgreich geloescht!" << endl << endl;
	}
	else
	{
		cout << "\nDer gesuchte Name konnte nicht im Adressverzeichniss gefunden werden, bitte wiederholen Sie ggf. den Vorgang." << endl << endl;
	}

	readFile(Kopf);

}



void showMenu(Adresse* Kopf)
{
	int menuInput = 0;

	while (true)
	{
		printMenu();
		cin >> menuInput;

		switch (menuInput)
		{
		case 1:
			addAdress(Kopf);
			break;

		case 2:
			changeAdress(Kopf);
			break;

		case 3:
			deleteAdress(Kopf);
			break;

		case 4:
			printAllAdress(Kopf);
			break;

		default:
			break;
		}
		if (menuInput == 0) {
			break;
		}
	}
}

int main()
{
	//Adresse adressen;
	Adresse* Kopf;
	Kopf = new Adresse;
	Kopf->vor = nullptr;
	Kopf->nach = nullptr;
	strcpy_s(Kopf->name, 30, "Mueller");

	readFile(Kopf);
	showMenu(Kopf);

	saveIntoFile(Kopf);

	system("pause");
	return 0;
}