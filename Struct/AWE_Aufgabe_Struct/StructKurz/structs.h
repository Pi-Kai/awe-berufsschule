#pragma once
#include <string>
#include <vector>

using namespace std;

struct firma
{
	string nameFirma;
	string adresseFirma;
	int telefonFirma;
	string ausbilder;
};

struct unterrichtsfaecher
{
	string name;
	string tag;
	int stunde;
	string lehrer;
	string raum;
};

struct schueler
{
	string nameSchueler;
	string vornameSchueler;
	string klasse;
	firma _firma;
	vector<unterrichtsfaecher> unterricht;
};