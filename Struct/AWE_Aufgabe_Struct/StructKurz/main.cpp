#include <iostream>
#include "structs.h"
#include "Eingabe.h"
#include "Ausgabe.h"

using namespace std; 


int main()
{
	bool listeKomplett = false;
	vector<schueler> schuelerliste;
	int i = 0;
	string temp = "";
	char test = '0';

	cout << "Testdurchlauf? y/n" << endl;
	cin >> test;

	if (test == 'y')
	{
		schuelerliste.push_back(schueler());
		schuelerliste.push_back(schueler());

		testInput(schuelerliste[0], schuelerliste[1]);

		Ausgabe(schuelerliste[0], schuelerliste[0].unterricht.size());
		Ausgabe(schuelerliste[1], schuelerliste[1].unterricht.size());
	}

	else
	{
		while (i < 15)
		{

			schuelerliste.push_back(schueler());
			getInput(schuelerliste[i]);

			cout << "Ist die Liste der Schueler komplett? Wenn ja, geben Sie bitte ' leer ' ein." << endl;
			cin >> temp;

			if (temp == "leer") break;

			i++;
		}


		for (int k = 0; k < schuelerliste.size(); k++)
		{
			Ausgabe(schuelerliste[k], k);
		}
	}
	system("pause");
	return 0;
}