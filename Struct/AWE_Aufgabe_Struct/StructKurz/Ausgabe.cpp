#include "Ausgabe.h"
#include "structs.h"
#include <iostream>
#include <iomanip>

using namespace std;

void Ausgabe(schueler& schueler, int i)
{
	cout << left << setw(15) << "Nachname: " << schueler.nameSchueler << endl;
	cout << left << setw(15) << "Vorname: " << schueler.vornameSchueler << endl;
	cout << left << setw(15) << "Klasse: " << schueler.klasse << "\n" << endl;
				 		 
	cout << left << setw(15) << "Firmenname: " << schueler._firma.nameFirma << endl;
	cout << left << setw(15) << "Adresse: " << schueler._firma.adresseFirma << endl;
	cout << left << setw(15) << "Telefonnummer: " << schueler._firma.telefonFirma << endl;
	cout << left << setw(15) << "Ausbilder : " << schueler._firma.ausbilder << endl;

	for (int k = 0; k < i; k++)
	{
		cout << "\nFolgende F�cher sind belegt: \n" << endl;
		cout << "Name des Faches: " << schueler.unterricht[k].name << endl;
		cout << "Tag an dem " << schueler.unterricht[k].name << " stattfindet: " << schueler.unterricht[k].tag << endl;
		cout << schueler.unterricht[k].name << " findet in der " << schueler.unterricht[k].stunde << ". Stunde statt." << endl;
		cout << "Der Lehrer ist: " << schueler.unterricht[k].lehrer << endl;
		cout << "Der Unterricht findet im Raum " << schueler.unterricht[k].raum << " statt.\n" << endl;
	}
}