#include "Eingabe.h"
#include <iostream>
#include "structs.h"

using namespace std;



void getInput(schueler& schueler)
{
	bool anzahlFaecherErreicht = false;

	cout << "Wie lautet der Nachname des Schuelers?" << endl;
	cin >> schueler.nameSchueler;

	cout << "Wie lautet der Vorname des Schuelers?" << endl;
	cin >> schueler.vornameSchueler;

	cout << "In welche Klasse geht der Schueler?" << endl;
	cin >> schueler.klasse;

	cout << "Bei welcher Firma arbeitet er?" << endl;
	cin >> schueler._firma.nameFirma;

	cout << "Wie lautet die Adresse der Firma?" << endl;
	cin >> schueler._firma.adresseFirma;

	cout << "Wie lautet die Telefonnummer der Firma?" << endl;
	cin >> schueler._firma.telefonFirma;

	cout << "Wie hei�t der Ausbilder des Schuelers?" << endl;
	cin >> schueler._firma.ausbilder;

	cout << "Bitte geben Sie die F�cher ein:\n(Wenn Sie alle F�cher eingegeben haben, geben Sie bitte ' fertig ' ein.) " << endl;

	while (anzahlFaecherErreicht == false)
	{
		int k = 0;

		schueler.unterricht.push_back(unterrichtsfaecher());

		cout << "Name des Faches: " << endl;
		cin >> schueler.unterricht[k].name;

		cout << "An welchem Tag wird " << schueler.unterricht[k].name << " behandelt? " << endl;
		cin >> schueler.unterricht[k].tag;

		cout << "In der wievielten Stunde wird  " << schueler.unterricht[k].name << " behandelt? " << endl;
		cin >> schueler.unterricht[k].stunde;

		cout << "Welcher Lehrer unterrichtet " << schueler.unterricht[k].name << " ? " << endl;
		cin >> schueler.unterricht[k].lehrer;

		cout << "In welchem Raum findet " << schueler.unterricht[k].name << " statt? " << endl;
		cin >> schueler.unterricht[k].raum;

		char weitermachen;
		cout << "Weiteres Fach eingeben? y/n" << endl;
		cin >> weitermachen;
		if (weitermachen == 'n') break;

		k++;

		

	}
}


void testInput(schueler& schueler1, schueler& schueler2)
{
	schueler1.nameSchueler = "haberland";
	schueler1.vornameSchueler = "kai";
	schueler1.klasse = "itb93";
	schueler1._firma.nameFirma = "duecker";
	schueler1._firma.adresseFirma = "jaaaaa";
	schueler1._firma.telefonFirma = 1337;
	schueler1._firma.ausbilder = "peter";

	schueler1.unterricht.push_back(unterrichtsfaecher());

	schueler1.unterricht[0].name = "mathe";
	schueler1.unterricht[0].tag = "donnerstag";
	schueler1.unterricht[0].stunde = 3;
	schueler1.unterricht[0].lehrer = "epping";
	schueler1.unterricht[0].raum = "ab20";

	schueler1.unterricht.push_back(unterrichtsfaecher());

	schueler1.unterricht[1].name = "deutsch";
	schueler1.unterricht[1].tag = "freitag";
	schueler1.unterricht[1].stunde = 2;
	schueler1.unterricht[1].lehrer = "epping";
	schueler1.unterricht[1].raum = "ab20";



	schueler2.nameSchueler = "haberland";
	schueler2.vornameSchueler = "sonja";
	schueler2.klasse = "itb89";
	schueler2._firma.nameFirma = "jauuu";
	schueler2._firma.adresseFirma = "lool";
	schueler2._firma.telefonFirma = 6969;
	schueler2._firma.ausbilder = "skrr";
			
	schueler2.unterricht.push_back(unterrichtsfaecher());
			
	schueler2.unterricht[0].name = "qewrwt";
	schueler2.unterricht[0].tag = "trew";
	schueler2.unterricht[0].stunde = 23;
	schueler2.unterricht[0].lehrer = "aert";
	schueler2.unterricht[0].raum = "qeart";
			
	schueler2.unterricht.push_back(unterrichtsfaecher());
			
	schueler2.unterricht[1].name = "deutsch";
	schueler2.unterricht[1].tag = "freitag";
	schueler2.unterricht[1].stunde = 2;
	schueler2.unterricht[1].lehrer = "epping";
	schueler2.unterricht[1].raum = "ab20";
}