#include "Abfrage.h"
#include <iostream>
#include <set>
#include <ctime>
#include <fstream>

using namespace std;

string getLineNrX(const int& x, ifstream& vokabeln)
{
    string line;
    int lineno = 0;
    while (getline(vokabeln, line))
    {
        lineno++;
        if (lineno == x)
        {
            vokabeln.clear();
            vokabeln.seekg(0);
            return line;
        }
    }

}


int zaehleZeilenInDatei(ifstream& vokabeln)
{
    int tempZeilen = 0;
    string textTemp;
    while (getline(vokabeln, textTemp))
    {
        tempZeilen++;
    }
    vokabeln.clear();
    vokabeln.seekg(0);
    return tempZeilen;
}


void abfrage(int& anzahlVokabeln, pair<string, string>* &vokabelPaar, const char& languageChoice)
{

    bool schonVorhanden = false;
    int random_variable = 0;
    int richtigeAngaben = 0;
    set<int> alleVokabeln = {};
    int j = 0;

    ifstream vokabeln("vokabeln.txt");

    int anzahlZeilenInDatei = zaehleZeilenInDatei(vokabeln);

    srand((unsigned)time(NULL));


    if (languageChoice == 'd')
    {
        while (alleVokabeln.size() != anzahlZeilenInDatei/2)//anzahlVokabeln)
        {
            schonVorhanden = false;
            string temp;

            random_variable = rand() % (anzahlZeilenInDatei/2+1);//anzahlVokabeln;
            if (random_variable == 0)
            {
                random_variable = 1;
            }

            for (auto k : alleVokabeln)
            {
                if (k == random_variable)
                {
                    schonVorhanden = true;
                }
            }


            alleVokabeln.insert(random_variable);

            if (schonVorhanden == false)
            {
                int a = (random_variable * 2 - 1);
                int b = (random_variable * 2);

                if (random_variable == 0)
                {
                    a = 1;
                    b = 2;
                }


                string fileTempDE = getLineNrX(a, vokabeln);
                string fileTempEng = getLineNrX(b, vokabeln);

                std::cout << "Frage " << j + 1 << ": Wie lautet die Uebersetzung von " << fileTempDE << "?" << endl;
                cin >> temp;

                if (temp == fileTempEng) richtigeAngaben++;

                j++;
            }

        }

        std::cout << "Ende der Abfrage:\nSie haben " << richtigeAngaben << " von " << alleVokabeln.size() << " richtig beantwortet." << endl;

    }

    else if (languageChoice == 'e')
    {
        while (alleVokabeln.size() != anzahlZeilenInDatei / 2)
        {
            schonVorhanden = false;
            string temp;

            random_variable = rand() % (anzahlZeilenInDatei / 2 + 1);
            if (random_variable == 0)
            {
                random_variable = 1;
            }

            for (auto k : alleVokabeln)
            {
                if (k == random_variable)
                {
                    schonVorhanden = true;
                }
            }

            alleVokabeln.insert(random_variable);

            if (schonVorhanden == false)
            {

                int a = (random_variable * 2 - 1);
                int b = (random_variable * 2);

                if (random_variable == 0)
                {
                    a = 1;
                    b = 2;
                }


                string fileTempDE = getLineNrX(a, vokabeln);
                string fileTempEng = getLineNrX(b, vokabeln);


                std::cout << "Frage " << j + 1 << ": Wie lautet die Uebersetzung von " << fileTempEng << "?" << endl;
                cin >> temp;
                if (temp == fileTempDE) richtigeAngaben++;

                j++;
            }

        }

        vokabeln.close();
        std::cout << "Ende der Abfrage:\nSie haben " << richtigeAngaben << " von " << alleVokabeln.size() << " richtig beantwortet." << endl;
    }
}