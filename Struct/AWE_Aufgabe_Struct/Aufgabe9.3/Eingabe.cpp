#include "Eingabe.h"
#include <iostream>
#include <set>
#include <ctime>
#include <fstream>

using namespace std;

void eingabe(int &anzahlVokabeln,
    pair<string, string>* &vokabelPaar,
    char & languageChoice)
{


char anfuegen = '0';
cout << "Moechten Sie Vokabeln zu Ihrer Liste hinzufuegen? (y/n)" << endl;
cin >> anfuegen;

if (anfuegen == 'y')
{
    cout << "Wie viele Vokabeln wollen Sie eingeben?" << endl;
    cin >> anzahlVokabeln;

    ofstream vokabeln;
    vokabeln.open("vokabeln.txt", ios_base::app);


    vokabelPaar = new pair<string, string>[anzahlVokabeln];

    for (int i = 0; i < anzahlVokabeln; ++i)
    {
        cout << "\nVokabel " << i + 1 << " (deutsch) : ";
        cin >> vokabelPaar[i].first;
        vokabeln << vokabelPaar[i].first << endl;

        cout << "Vokabel " << i + 1 << " (englisch) : ";
        cin >> vokabelPaar[i].second;
        vokabeln << vokabelPaar[i].second << endl;

        cout << endl;



    }
    vokabeln.close();
}

cout << "\nStart der Abfrage:\nMoechten Sie in Deutsch oder Englisch gefragt werden? (d/e) " << endl;
cin >> languageChoice;

}