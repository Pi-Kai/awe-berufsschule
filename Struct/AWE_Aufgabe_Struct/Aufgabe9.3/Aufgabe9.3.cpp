#include <iostream>
#include <vector>
#include <ctime>
#include <set>
#include "Eingabe.h"
#include "Abfrage.h"

using namespace std;

int main()
{

    int anzahlVokabeln = 0;
    pair<string, string>* vokabelPaar = {};
    char languageChoice = '0';

    eingabe(anzahlVokabeln, vokabelPaar, languageChoice);
    abfrage(anzahlVokabeln, vokabelPaar, languageChoice);
 
    system("pause");
    return 0;
}

