#include <iostream>
#include <iomanip>

using namespace std;

struct TBenutzer
{
	char Name[40 + 1];
	char Passwort[40 + 1];
};


int main()
{
	bool logInKorrekt = false;
	while (logInKorrekt == false)
	{

		TBenutzer* benutzer = new TBenutzer;

		cout << "Bitte geben Sie Ihren Namen ein." << endl;
		cin >> benutzer->Name;
		int k = 0;
		int richtigeStellen = 0;


		cout << "Bitte geben Sie Ihr Passwort ein." << endl;
		cin >> benutzer->Passwort;

		char temp[40 + 1];
		for (int i = strlen(benutzer->Name)-1; i >= 0; --i, k++)
		{
			temp[k] = benutzer->Name[i];
			if (temp[k] == benutzer->Passwort[k])
			{
				richtigeStellen++;
			}
		}

		if (richtigeStellen == strlen(benutzer->Name))
		{
			cout << left << setw(10) << "Name" << setw(10) << "Passwort" << endl;
			cout << "------------------" << endl;
			cout << left << setw(10) << benutzer->Name << setw(10) << benutzer->Passwort << setw(40) << "Das Passwort ist korrekt!" << endl;


			//cout << "Das Passwort ist korrekt!" << endl;
			logInKorrekt = true;
		}
		else
		{
			cout << left << setw(10) << "Name" << setw(10) << "Passwort" << endl;
			cout << "------------------#" << endl;
			cout << left << setw(10) << benutzer->Name << setw(10) << benutzer->Passwort << setw(40) << "Das Passwort ist falsch! Versuchen Sie es erneut!" << endl;


			//cout << "Denied\Versuchen Sie es erneut!" << endl;
		}

	}

	system("pause");
	return 0;
}
 
