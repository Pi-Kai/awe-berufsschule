#include <iostream>
#include <vector>
#include <ctime>
#include <set>

using namespace std;

int main()
{

    int anzahlVokabeln;

    cout << "Wie viele Vokabeln wollen Sie eingeben?" << endl;
    cin >> anzahlVokabeln;

    pair<string, string>* vokabelPaar;

    vokabelPaar = new pair<string, string>[anzahlVokabeln];

    for (int i = 0; i < anzahlVokabeln; ++i)
    {
        cout << "\nVokabel " << i + 1 << " (deutsch) : ";
        cin >> vokabelPaar[i].first;

        cout << "Vokabel " << i + 1 << " (englisch) : ";
        cin >> vokabelPaar[i].second;

        cout << endl;
    }

    srand((unsigned)time(NULL));
    int random_variable;
    int richtigeAngaben = 0;
    int j = 0;
    bool schonVorhanden = false;

    set<int> alleVokabeln;

    char abfrage;
    cout << "\nStart der Abfrage:\nMoechten Sie in Deutsch oder Englisch gefragt werden? (d/e) " << endl;
    cin >> abfrage;

    if (abfrage == 'd')
    {
        while (alleVokabeln.size() != anzahlVokabeln)
        {
            schonVorhanden = false;
            string temp;

            random_variable = rand() % anzahlVokabeln;

            for (auto k : alleVokabeln)
            {
                if (k == random_variable)
                {
                    schonVorhanden = true;
                }
            }


            alleVokabeln.insert(random_variable);

            if (schonVorhanden == false)
            {
                cout << "Frage " << j + 1 << ": Wie lautet die Uebersetzung von " << vokabelPaar[random_variable].first << "?" << endl;
                cin >> temp;
                if (temp == vokabelPaar[random_variable].second) richtigeAngaben++;

                j++;
            }

        }

        cout << "Ende der Abfrage:\nSie haben " << richtigeAngaben << " von " << alleVokabeln.size() << " richtig beantwortet." << endl;

    }

    else if (abfrage == 'e')
    {
        while (alleVokabeln.size() != anzahlVokabeln)
        {
            schonVorhanden = false;
            string temp;

            random_variable = rand() % anzahlVokabeln;

            for (auto k : alleVokabeln)
            {
                if (k == random_variable)
                {
                    schonVorhanden = true;
                }
            }

            alleVokabeln.insert(random_variable);

            if (schonVorhanden == false)
            {
                cout << "Frage " << j + 1 << ": Wie lautet die Uebersetzung von " << vokabelPaar[random_variable].second << "?" << endl;
                cin >> temp;
                if (temp == vokabelPaar[random_variable].first) richtigeAngaben++;

                j++;
            }

        }

        cout << "Ende der Abfrage:\nSie haben " << richtigeAngaben << " von " << alleVokabeln.size() << " richtig beantwortet." << endl;
    }

    system("pause");
    return 0;
}

