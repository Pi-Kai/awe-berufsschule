#include "output.h"
#include <iostream>
#include <iomanip>
#include <fstream>
#include "Adresse.h"


using namespace std;

void printMenu()
{
	cout << "Willkommen zur Adressverwaltung!" << endl;
	cout << "--------------------------------" << endl;
	cout << "\nWas moechten Sie tun?" << endl;
	cout << "1. Neue Adresse hinzufuegen" << endl;
	cout << "2. Eine Adresse aendern" << endl;
	cout << "3. Eine Adresse loeschen" << endl;
	cout << "4. Alle Adressen ausgeben" << endl;
	cout << "0. Programm beenden" << endl;
}


void printAllAdress(Adresse* Kopf)
{
    cout << "### Alle Adressen:" << endl;
    cout << "##################" << endl << endl;

    Adresse* Laeufer;
    Laeufer = Kopf;

    while (Laeufer->nach) {
        Laeufer = Laeufer->nach;

        cout << Laeufer->name << endl;
        cout << Laeufer->vorname << endl;
        cout << Laeufer->strasse << endl;
        cout << Laeufer->plz << endl;
        cout << Laeufer->ort << endl;
        cout << Laeufer->id << endl << endl;
    }
}

void saveIntoFile(Adresse* Kopf)
{
    Adresse* Laeufer, *Dummy;
    Laeufer = Kopf;

    do {
        Dummy = Laeufer;
        Laeufer = Laeufer->nach;
    } while (Laeufer);

    ofstream fileWriter;
    fileWriter.open("Adressliste.txt", std::ofstream::out | std::ofstream::app);

    fileWriter << endl << Dummy->name << ";" << Dummy->vorname << ";" << Dummy->strasse << ";" << Dummy->plz << ";" << Dummy->ort << ";" << Dummy->id << ";" ;

    fileWriter.close();
}
