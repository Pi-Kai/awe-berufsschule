#pragma once
#include "Adresse.h"

void showMenu(Adresse* Kopf);
void addAdress(Adresse* Kopf);
void changeAdress(Adresse* Kopf);
void deleteAdress(Adresse* Kopf);
void readFile(Adresse* Kopf);