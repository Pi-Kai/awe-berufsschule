#pragma once

struct Adresse
{
	char name[30];
	char vorname[30];
	char strasse[30];
	char plz[10];
	char ort[30];
	int id;
	Adresse* vor;
	Adresse* nach;
};