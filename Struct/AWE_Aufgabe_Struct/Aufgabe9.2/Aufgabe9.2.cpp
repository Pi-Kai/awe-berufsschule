#include <iomanip>
#include <iostream>

using namespace std;


struct TSchueler
{
	char Name[20 + 1];
	char Klasse[5 + 1];
	int Noten[3];
};

int main()
{
	TSchueler schueler;

	TSchueler* aschueler = new TSchueler;
	TSchueler* bschueler = new TSchueler;
	TSchueler* cschueler = new TSchueler;

	cout << "Bitte geben Sie den Namen ein. " << endl;
	cin >> (*aschueler).Name;

	cout << "Bitte geben Sie die Klasse ein. " << endl;
	cin >> (*aschueler).Klasse;

	cout << "Bitte geben Sie die Noten ein." << endl;
	cout << "Mathe: ";
	cin >> (*aschueler).Noten[0];
	cout << "Deutsch: ";
	cin >> (*aschueler).Noten[1];
	cout << "Englisch: ";
	cin >> (*aschueler).Noten[2];

	cout << "Bitte geben Sie den Namen ein. " << endl;
	cin >> (*bschueler).Name;

	cout << "Bitte geben Sie die Klasse ein. " << endl;
	cin >> (*bschueler).Klasse;

	cout << "Bitte geben Sie die Noten ein." << endl;
	cout << "Mathe: ";
	cin >> (*bschueler).Noten[0];
	cout << "Deutsch: ";
	cin >> (*bschueler).Noten[1];
	cout << "Englisch: ";
	cin >> (*bschueler).Noten[2];

	cout << "Bitte geben Sie den Namen ein. " << endl;
	cin >> (*cschueler).Name;

	cout << "Bitte geben Sie die Klasse ein. " << endl;
	cin >> (*cschueler).Klasse;

	cout << "Bitte geben Sie die Noten ein." << endl;
	cout << "Mathe: ";
	cin >> (*cschueler).Noten[0];
	cout << "Deutsch: ";
	cin >> (*cschueler).Noten[1];
	cout << "Englisch: ";
	cin >> (*cschueler).Noten[2];


	cout << setw(10) << "Name: " << setw(20) << "Klasse: " << setw(8) << "M" << setw(4) << "D" << setw(4) << "E" << setw(10) << "Schnitt" << endl;
	cout << "---------------------------------------------------------------------------------------------------------------------" << endl;

	double schnittA = (aschueler->Noten[0] + aschueler->Noten[1] + aschueler->Noten[2]) / 3.0;
	double schnittB = (bschueler->Noten[0] + bschueler->Noten[1] + bschueler->Noten[2]) / 3.0;
	double schnittC = (cschueler->Noten[0] + cschueler->Noten[1] + cschueler->Noten[2]) / 3.0;

	cout << setw(10) << aschueler->Name << setw(20) << aschueler->Klasse << setw(8) << aschueler->Noten[0] << setw(4) << aschueler->Noten[1] << setw(4) << aschueler->Noten[2] << setw(10) << schnittA << endl;
	cout << setw(10) << bschueler->Name << setw(20) << bschueler->Klasse << setw(8) << bschueler->Noten[0] << setw(4) << bschueler->Noten[1] << setw(4) << bschueler->Noten[2] << setw(10) << schnittB << endl;
	cout << setw(10) << cschueler->Name << setw(20) << cschueler->Klasse << setw(8) << cschueler->Noten[0] << setw(4) << cschueler->Noten[1] << setw(4) << cschueler->Noten[2] << setw(10) << schnittC << endl;
}

