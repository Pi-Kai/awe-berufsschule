#include<iostream>
#include<cstring>
#include<ctime>
#include<iomanip>

using namespace std;

struct Adresse {
    int id;
    char strasse[21];
    char hausnummer[6];
    char plz[6];
    char stadt[21];
    Adresse *vor;
    Adresse *nach;
};

void adresseEinlesen(Adresse *Kopf) {
    Adresse *Laeufer, *Dummy;
    Laeufer = Kopf;

    cout << "### Neue Adresse:" << endl;
    cout << "#################" << endl << endl;

    // ans Ende der Liste laufen
    do {
        Dummy = Laeufer;
        Laeufer = Laeufer->nach;
    } while (Laeufer);

    Laeufer = new Adresse;
    Dummy->nach = Laeufer;
    Laeufer->id = rand() % 1000;
    Laeufer->vor = Dummy;
    Laeufer->nach = nullptr;

    cout << "Bitte Strasse eingeben: ";
    cin >> Laeufer->strasse;
    cout << "Bitte Hausnummer eingeben: ";
    cin >> Laeufer->hausnummer;
    cout << "Bitte Postleitzahl eingeben: ";
    cin >> Laeufer->plz;
    cout << "Bitte Stadt eingeben: ";
    cin >> Laeufer->stadt;

    cout << endl << "### Neue Adresse wurde erfolgreich hinzugefuegt!" << endl << endl;
}

void adressenAusgeben(Adresse *Kopf) {
    cout << "### Alle Adressen:" << endl;
    cout << "##################" << endl << endl;

    Adresse *Laeufer;
    Laeufer = Kopf;

    // Tabellenkopf
    cout << setw(5) << setfill(' ') << left << "ID" 
         << setw(25) << setfill(' ') << left <<  "Strasse" 
         << setw(12) << setfill(' ') << left << "Hausnummer" 
         << setw(7) << setfill(' ') << left << "PLZ" 
         << setw(20) << setfill(' ') << left <<  "Stadt" << endl;
    cout << "--------------------------------------------------------------" << endl;
    
    // Zeilen
    while(Laeufer->nach) {
        Laeufer = Laeufer->nach;
        cout << setw(5) << setfill(' ') << left << Laeufer->id 
             << setw(25) << setfill(' ') << left << Laeufer->strasse 
             << setw(12) << setfill(' ') << left << Laeufer->hausnummer 
             << setw(7) << setfill(' ') << left << Laeufer->plz
             << setw(20) << setfill(' ') << left << Laeufer->stadt << endl;
    }
    cout << endl << endl;
}

void adresseAendern(Adresse *Kopf) {
    cout << "### Adresse aendern:" << endl;
    cout << "####################" << endl << endl;

    Adresse *Laeufer;
    Laeufer = Kopf;
    int id;
    bool found = false;

    adressenAusgeben(Kopf);
    cout << "Bitte ID eingeben, um Adresse zu aendern: ";
    cin >> id;

    while(Laeufer->nach) {
        Laeufer = Laeufer->nach;
        if(Laeufer->id == id) {
            cout << "Bitte neue Strasse eingeben: ";
            cin >> Laeufer->strasse;
            cout << "Bitte neue Hausnummer eingeben: ";
            cin >> Laeufer->hausnummer;
            cout << "Bitte neue Postleitzahl eingeben: ";
            cin >> Laeufer->plz;
            cout << "Bitte neue Stadt eingeben: ";
            cin >> Laeufer->stadt;
            found = true;
            break;
        }
    }

    if(found) {
        cout << endl << "### Adresse wurde erfolgreich geaendert!" << endl << endl;
    } else {
        cout << endl << "### Die angegebene Adresse konnte nicht gefunden werden!" << endl << endl;
    }
}

void adresseLoeschen(Adresse *Kopf) {
    cout << "### Adresse loeschen:" << endl;
    cout << "####################" << endl << endl;

    Adresse *Laeufer, *Dummy;
    Laeufer = Kopf;
    int id;
    bool found = false;

    adressenAusgeben(Kopf);
    cout << "Bitte ID eingeben, um Adresse zu loeschen: ";
    cin >> id;

    while(Laeufer->nach) {
        Dummy = Laeufer;
        Laeufer = Laeufer->nach;

        // Umverlinken bei Uebereinstimmung
        if(Laeufer->id == id) {
            Dummy->nach = Laeufer->nach;
            if(Laeufer->nach) {
                Laeufer->nach->vor = Dummy;
            }
            delete[] Laeufer;
            found = true;
            break;
        }
    }

    if(found) {
        cout << "### Adresse wurde erfolgreich geloescht!" << endl << endl;
    } else {
        cout << "### Die angegebene Adresse konnte nicht gefunden werden!" << endl << endl;
    }
}

void speicherFreigeben(Adresse *Kopf) {
    Adresse *Laeufer, *Dummy;
    Laeufer = Kopf;

    while(Laeufer->nach) {
        Dummy = Laeufer->nach;
        delete[] Laeufer;
        Laeufer = Dummy;
    }

    cout << "### Speicherfreigabe erfolgreich!" << endl;
}

int main() {
    srand(unsigned(time(NULL)));

    Adresse *Kopf;
    Kopf = new Adresse;
    Kopf->vor = nullptr;
    Kopf->nach = nullptr;
    strcpy(Kopf->strasse, "Kein Inhalt");

    cout << "### Willkommen zur Adressverwaltung" << endl;
    cout << "###################################" << endl << endl;

    int menuEingabe = 0;
    while(true) {
        cout << "Was wollen Sie als naechstes tun?" << endl;
        cout << "1. Neue Adresse einlesen." << endl;
        cout << "2. Alle Adressen ausgeben." << endl;
        cout << "3. Eine Adresse aendern." << endl;
        cout << "4. Eine Adresse loeschen." << endl;
        cout << "0. Programm verlassen." << endl << endl;
        cout << "Ihre Eingabe: ";
        cin >> menuEingabe;

        switch (menuEingabe) {
        case 1:
            adresseEinlesen(Kopf);
            break;
        case 2:
            adressenAusgeben(Kopf);
            break;
        case 3:
            adresseAendern(Kopf);
            break;
        case 4:
            adresseLoeschen(Kopf);
            break;
        default:
            break;
        }
        if(menuEingabe == 0) {
            break;
        }
    }

    speicherFreigeben(Kopf);

    delete Kopf;

    cout << "### Das Programmende wurde beendet." << endl;
    return 0;
}